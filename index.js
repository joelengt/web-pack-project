// Guia de uso rapido
//https://webpack.github.io/docs/tutorials/getting-started/

// Instalar
npm install webpack -g

// iniciar proyecto
npm init 

// SETUP THE COMPILATION

// Create these files:

entry.js 
// Tner un .js con codigo y con este contenido -> document.write("It works.");

webpack ./entry.js bundle.js


> Crear modulos con js (ECMAScript5)

// content.js 
module.exports = "It works from content.js.";

// index.js 
require('./content.js');

// Loaders -> puedo usar css, styls, .jade como modulo de js para cargar al html
// Añadir css 
require("!style!css!./index.css");

// Llamar  archivos css sin el require de extension 
webpack ./index.js bundle.js --module-bind 'css=style!css'



// A CONFIG FILE
// crear file 
webpack.config.js 

// dentro 
module.exports = {
    entry: "./index.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};

// ejecutar 
webpack


// Si el proyecto es muy grande y requiere barra de progreso y colores
webpack --progress --colors

// WATCH MODE - Compilacion automatica al guardar
webpack --progress --colors --watch


// DEVELOPMENT SERVER
npm install webpack-dev-server -g

webpack-dev-server --progress --colors

--> corriendo en http://localhost:8080
--> watch en http://localhost:8080/webpack-dev-server/bundle  (sale en tiempo real los cambios)
-> ver wacth en http://localhost:8080/webpack-dev-server


// Why WebPack is the better?

> Code Splitting

webpack has two types of dependencies in its dependency tree: sync and async. Async dependencies act as split points and form a new chunk. After the chunk tree is optimized, a file is emitted for each chunk.

Read more about Code Splitting.

> Loaders

webpack can only process JavaScript natively, but loaders are used to transform other resources into JavaScript. By doing so, every resource forms a module.
Read more about Using loaders and Loaders.

* Puedo requerir como modulo a mi main.css, main.jade etc 

